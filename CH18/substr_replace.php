<?php
$str = "ABCDEFGHIJKL";

$rep = "XYZ";

echo substr_replace($str, $rep, '0'); //XYZ
echo substr_replace($str, $rep, '5');   //ABCDEXYZ
echo substr_replace($str, $rep, 5, 3);   //ABCDEXYZIJKL
echo substr_replace($str, $rep, -1, 3);   //ABCDEFGHIJKXYZ
echo substr_replace($str, $rep, -4, 3);   //ABCDEFGHXYZL