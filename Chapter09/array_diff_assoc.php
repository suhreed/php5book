<?php
//Our first array
$array1 = array("a" => "green", "b" => "brown", "c" => "blue", "red");

//our second array
$array2 = array("a" => "green", "yellow", "red");

//finding difference
$result = array_diff_assoc($array1, $array2);

//Let's see reult
print_r($result);

/* Result should be:

Array
(
    [b] => brown
    [c] => blue
    [0] => red
)
*/
?>