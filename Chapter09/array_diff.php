<?php
//Our arrays
$array1 = array("a" => "green", "red", "blue", "red");
$array2 = array("b" => "green", "yellow", "red");

//getting differecne
$result = array_diff($array1, $array2);

//Let's see result
print_r($result);

/* Result should be:

Array
(
    [1] => blue
)

*/
?>