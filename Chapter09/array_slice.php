<?php

//original array
$big = array("a", "b","c","d","e","f","g");

//let's make a small array by slicing the big array
$small = array_slice($big, 2, 3);

//Do looping to see result
foreach ($small as $var ) {
	print "$var \n";
	}
	
?>