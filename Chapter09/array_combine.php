<?php

//here is our first array
$a = array('green', 'red', 'yellow');

//here is our second array
$b = array('mango', 'apple', 'banana');

//now combining both arrays to form a third array
$c = array_combine($a, $b);

//Let's see what we have in third array
print_r($c);

/* Result should be:

Array
(
    [green] => mango
    [red] => apple
    [yellow] => banana
)

*/


?>