<?php

//our array
$fruits = array("d" => "lemon",
                "a" => "orange",
                "b" => "banana",
                "c" => "apple");

//sorting the array with asort
 asort($fruits);

//Looping through the array to see key-value pair
foreach ($fruits as $key => $val) {
   		echo "$key = $val\n";
      }

/*
Result will be:
c = apple
b = banana
d = lemon
a = orange

*/
?>