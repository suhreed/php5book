<?php

//our arrays
$array1 = array("a" => "green", "red", "blue");
$array2 = array("b" => "green", "yellow", "red");

//getting intersect
$result = array_intersect($array1, $array2);


//showing result
print_r($result);

/*
result is;
Array
(
    [a] => green
    [0] => red
)


*/
?>