<?php

//session starts
session_start();


if (!isset($_SESSION['name']) && !isset($_POST['name'])) {

//if session variables do not exist then prints the following form
echo <<<EOD
	<form action="$_SERVER[PHP_SELF]" method="post">
	Name: <input type="text" name="name">
	<input type="submit" name="submit" value="Enter your name">
	</form>
EOD;

} else if (!isset($_SESSION['name']) & isset($_POST['name'])) {

	if (!empty($_POST['name'])) {
		$_SESSION['name'] = $_POST['name'];
		$_SESSION['start'] = time();

		echo "Welcome, " . $_POST['name']
			 . ". A new session has been activated for you. Click <a href="
			 . $_SERVER['PHP_SELF'] . "> here </a> to refresh the page.";
	} else {
		echo "ERROR: Please enter your name!";
	}
} else if (isset($_SESSION['name'])) {

	echo "Welcome back, " . $_SESSION['name'] . ". This session was activated "
			. round((time() - $_SESSION['start']) / 60)
			. " minute(s) ago. Click <a href=" . $_SERVER['PHP_SELF']
			. ">here</a> to refresh the page.";
	echo "Your session ID is: ".session_id();
}
